//
//  main.swift
//  lw7
//
//  Created by Daniil Lushnikov on 27.09.2021.
//  List of students who would be expired from university

// Set of students names
var invitedStudents = Set<String>()
var studentsWhoCame = Set<String>()
// n - amount of came students
let n = Int(readLine() ?? "") ?? 0
// here we collect names of students who came to exam
for _ in 0..<n {
    studentsWhoCame.insert(readLine() ?? "")
}
// m - amount of invited students
let m = Int(readLine() ?? "") ?? 0
// here we collect names of students who were invited to exam
for _ in 0..<m {
    invitedStudents.insert(readLine() ?? "")
}
// here we print our list
for elements in invitedStudents.subtracting(studentsWhoCame) {
    print(elements)
}
